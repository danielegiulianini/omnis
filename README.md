# omnis: **O**ntology for **M**useum **N**avigation and **I**tinerary **S**election
Project for the course "Semantic Web" of Ingegneria e Scienze Informatiche, University of Bologna, Cesena Campus, 2021.

This repository contains:

1. OMNIS, an ontology, written in OWL 2.0 and SWRL, for personalization of tours through museum rooms with respect to visitor artistic preferences, capabilities or disabilities, if any.

2. a use case applied to The Uffizi Gallery of Firenze to show ontologies features. The use case is implemented in java 1.8, leveraging Apache Jena semantic web framework and Openllet reasoner.


For ontology to be opened in Protegè editor, refer to: [link](src/main/resources/ontologies/omnis/)

For running the use case, refer to: [link](src/main/java/sw/OmnisManager.java)

