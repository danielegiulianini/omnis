package sw.utils;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ArrayUtils {
	public static int[] appendElemToArrayOfPrimitiveInts(int[] array, int elem) {
		List<Integer> al = Arrays.stream(array).boxed().collect(Collectors.toList());
		al.add(elem);
		return al.stream().mapToInt(Integer::intValue).toArray();
	}
	
	public static double[] appendElemToArrayOfPrimitiveDoubles(double[] array, double elem) {
		List<Double> al = Arrays.stream(array).boxed().collect(Collectors.toList());
		al.add(elem);
		return al.stream().mapToDouble(Double::doubleValue).toArray();
	}
}
