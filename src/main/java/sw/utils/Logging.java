package sw.utils;

public class Logging {
	public static void logln(String msg) {
		System.out.println(msg);
	}

	public static void logln() {
		System.out.println();
	}

	public static void log(String msg) {
		System.out.print(msg);
	}

	public static void printTwoDimensionalArrayOfPrimitiveDoubles(double[][] arrayOfPrimitiveDoubles) {
		for (double[] array: arrayOfPrimitiveDoubles){
			for (double element : array) {
				log(element + ",\t\t");
			}
			logln();
		}
	}

	public static void printTwoDimensionalArrayOfPrimitiveInts(int[][] arrayOfPrimitiveDoubles) {
		for (int[] array: arrayOfPrimitiveDoubles){
			for (int element : array) {
				log(element + ",\t\t");
			}
			logln();
		}
	}
	public static void printArrayOfPrimitiveBooleans(boolean[] arrayOfPrimitiveDoubles) {

		for (int i=0; i<arrayOfPrimitiveDoubles.length; i++) {
			log(arrayOfPrimitiveDoubles[i] +" \t");
		}
		logln();
	}
	
	public static void printArrayOfPrimitiveInts(int[] arrayOfPrimitiveInts) {

		for (int i=0; i<arrayOfPrimitiveInts.length; i++) {
			log("["+i +"]:"+ arrayOfPrimitiveInts[i] +" \t");
		}
		logln();
	}
	
	public static void printArrayOfPrimitiveDoubles(double[] arrayOfPrimitiveDoubles) {

		for (int i=0; i<arrayOfPrimitiveDoubles.length; i++) {
			log("["+i +"]:"+ arrayOfPrimitiveDoubles[i] +" \t");
		}
		logln();
	}
}
