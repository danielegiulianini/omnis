package sw.utils;

import org.apache.commons.lang3.StringUtils;

public class MyStringUtils {
	
	public static String rpad(String inStr, int maxLength) {
		return (inStr + StringUtils.repeat(' ', maxLength)	//"                          " // typically a sufficient length spaces string.
				).substring(0, maxLength);
	}
	
	public static String prepend(String toBePrepended, String toPrepend) {
		StringBuilder sb = new StringBuilder(toBePrepended);
	    sb.insert(0, toPrepend);
	    return sb.toString();
	}
}
