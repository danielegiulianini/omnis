package sw.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;

public class JenaQueryUtils {
	
	public static Set<String> getResultQueryAsSet(String queryString, Model model, String questionMarkPrefixedVariableName) {
		Set<String> resourcesSet = new HashSet<>();
		fillCollectionWithQueryResults(resourcesSet,  queryString,  model,  questionMarkPrefixedVariableName);
		return resourcesSet;
	}
	
	private static <T> void fillCollectionWithQueryResults(Collection<String> resourcesList, String queryString, Model model, String questionMarkPrefixedVariableName) {
		Query query = QueryFactory.create(queryString);
		try (QueryExecution qexec = QueryExecutionFactory.create(query, model)) {
			ResultSet results = qexec.execSelect();
			while (results.hasNext()){
				QuerySolution soln = results.nextSolution();
				String mustVisitRoom = soln.get(questionMarkPrefixedVariableName).toString();
				resourcesList.add(mustVisitRoom);
			}
		}
	}
	
	public static List<String>  getResultQueryAsList(String queryString, Model model, String questionMarkPrefixedVariableName) {
		List<String> resourcesList = new ArrayList<>();
		fillCollectionWithQueryResults(resourcesList,  queryString,  model,  questionMarkPrefixedVariableName);
		return resourcesList;
	}
}
