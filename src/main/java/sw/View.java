package sw;

import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import sw.routing.MuseumTourComputer.PersonalizedTour;

public class View {

	private JFrame frame;
	private JPanel panel;
	private JTextArea textArea;

	public void initialize() {

		this.panel = new JPanel();

		panel.setLayout(new BorderLayout());
		panel.setPreferredSize(new Dimension(700, 400));

		this.textArea = new JTextArea("OMNIS use case applied to Uffizi Museum of Firenze\n\n\n");
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);

		JScrollPane pane = new JScrollPane(textArea);
		pane.setPreferredSize(new Dimension(700, 400));
		pane.setVerticalScrollBarPolicy(
				JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

		this.panel.add(pane, BorderLayout.CENTER);

		this.panel.add(pane, BorderLayout.CENTER);
		panel.setOpaque(true);

		this.frame = new JFrame("OMNIS use case");
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.setContentPane(this.panel);
		frame.pack();
	}

	public void display() {
		frame.setVisible(true);
	}

	public void printStartClassifying() {
		SwingUtilities.invokeLater( () -> this.textArea.append("classifying..."));
	}
	
	public void printEndClassifying() {
		SwingUtilities.invokeLater( () -> this.textArea.append("...end classifying.\n\n"));
	}
	
	public void printStartComputingFor(String user) {
		SwingUtilities.invokeLater( () -> this.textArea.append("Computing tour for user " + user + " ..."));
	}
	
	public void printTour(String user, PersonalizedTour tour) {
		SwingUtilities.invokeLater( () -> this.textArea.append("...done. Personalized tour for user "+ user + " between Uffizi rooms is: "
				+ tour.getPath() + "\n\nand passing through " + tour.getMustSees() + "\n\n\n\n"));
	}
	
	public void printEndPersonalization() {
		SwingUtilities.invokeLater(() -> this.textArea.append("Personalization complete."));
	}
}