package sw;

import static sw.utils.Logging.logln;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.jena.ontology.OntDocumentManager;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.mindswap.pellet.jena.ModelExtractor;
import org.mindswap.pellet.jena.PelletReasonerFactory;

import static sw.utils.JenaQueryUtils.getResultQueryAsList;
import static sw.utils.JenaQueryUtils.getResultQueryAsSet;
import static sw.utils.MyStringUtils.prepend;


import sw.routing.GraphBuilder;
import sw.routing.GraphBuilder.Graph;
import sw.routing.MuseumTourComputer;
import sw.routing.MuseumTourComputer.PersonalizedTour;

public class OmnisManager {

	private final String ontologyRelativePath = "/ontologies/omnis/omnis.owl";

	private final OntModel baseModel;

	private final String omnisUri = "http://www.semanticweb.org/daniele/ontologies/omnis/omnis#";	//used in sparql queries
	private final String omnisPrefix = "";

	private final String PREFIXES_STATEMENTS;

	private Model modelWithInferences;

	public OmnisManager() {

		this.baseModel = ModelFactory.createOntologyModel(PelletReasonerFactory.THE_SPEC);	//createOntologyModel();

		OntDocumentManager dm = baseModel.getDocumentManager();

		String accessibleOntUri = "http://www.AccessibleOntology.com/GenericOntology.owl" ;
		String erlangenOntUri = "http://erlangen-crm.org/current/";
		String ilocOntUri = "http://lod.nik.uni-obuda.hu/iloc#";
		String namespaceUri =  "http://lod.nik.uni-obuda.hu/iloc/iloc#";
		String dbpediaOntUri = "http://dbpedia.org/ontology/";
		String omOntUri = "http://www.ontology-of-units-of-measure.org/resource/om-2";
		String usoOntUri = "http://www.semanticweb.org/daniele/ontologies/uso/uso";
		String airoOntUri = "http://www.semanticweb.org/daniele/ontologies/airo/airo";
		String brailleOntUri = "http://www.AccessibleOntology.com/Braille.owl";
		String screenMagnifiersOntUri = "http://www.AccessibleOntology.com/ScreenMagnifiers";
		String screenReaderOntUri = "http://www.AccessibleOntology.com/ScreenReader.owl";

		//redirecting import to local file
		String pathToAccessibleOntology = "src/main/resources/ontologies/imported/accessible/GenericOntology.ttl" ;
		String pathToErlangen = "src/main/resources/ontologies/imported/erlangen/erlangen.owl";
		String pathToIloc = "src/main/resources/ontologies/imported/iloc/iloc.ttl";
		String pathToDbpedia = "src/main/resources/ontologies/imported/dbpedia/dbpedia.ttl";
		String pathToOm = "src/main/resources/ontologies/imported/om/om_reduced/om.ttl";
		String pathToUso = "src/main/resources/ontologies/uso/uso.ttl";
		String pathToAiro = "src/main/resources/ontologies/airo/airo.ttl";
		String pathToBraille ="src/main/resources/ontologies/imported/accessible/Braille.owl";
		String pathToScreenMagnifiers = "src/main/resources/ontologies/imported/accessible/ScreenMagnifiers.owl";
		String pathToScreenReader = "src/main/resources/ontologies/imported/accessible/ScreenReader.owl";

		dm.addAltEntry( accessibleOntUri,
				"file:" + pathToAccessibleOntology);
		dm.addAltEntry( erlangenOntUri,
				"file:" + pathToErlangen);
		dm.addAltEntry( ilocOntUri,
				"file:" + pathToIloc);
		dm.addAltEntry( dbpediaOntUri,
				"file:" + pathToDbpedia);
		dm.addAltEntry( omOntUri,
				"file:" + pathToOm);
		dm.addAltEntry( usoOntUri,
				"file:" + pathToUso);
		dm.addAltEntry( airoOntUri,
				"file:" + pathToAiro);

		dm.addAltEntry( brailleOntUri,
				"file:" + pathToBraille);
		dm.addAltEntry( screenMagnifiersOntUri,
				"file:" + pathToScreenMagnifiers);
		dm.addAltEntry( screenReaderOntUri,
				"file:" + pathToScreenReader);

		final InputStream in = OmnisManager.class.getResourceAsStream(ontologyRelativePath);
		baseModel.read(in, null, "TTL");

		this.PREFIXES_STATEMENTS = 
				"PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
						+ "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
						+ "PREFIX owl: <http://www.w3.org/2002/07/owl#>\n"
						+ "PREFIX iloc: <"+ namespaceUri+ ">\n"
						+ "PREFIX dbo: <"+ dbpediaOntUri +">\n"
						+ "PREFIX dbr: <"+ baseModel.getNsPrefixURI("dbr") +">\n"
						+ "PREFIX gold: <"+ baseModel.getNsPrefixURI("gold") +">\n"
						+ "PREFIX ecrm: <"+ erlangenOntUri +">\n"
						+ "PREFIX airo: <"+ airoOntUri +"#>\n"
						+ "PREFIX uso: <"+ usoOntUri +"#>\n"
						+ "PREFIX acc: <"+accessibleOntUri + "#>\n"
						+ "PREFIX om: <"+omOntUri + "/>\n"
						+ "PREFIX "+ omnisPrefix + ": <"+ omnisUri +">\n"
						+ "\n";
	}

	public void classify() {
		logln("begin classifying ...");

		ModelExtractor ext = new ModelExtractor(baseModel);
		modelWithInferences = ext.extractModel();

		logln("...end classifying.");
	}

	public PersonalizedTour computePersonalizedTourForUserInMuseum(String userIri, String museumIri) {

		//build graph
		Graph g = buildGraphForUserInMuseum(userIri, museumIri);

		EntrancesAndExits ee = findEntrancesAndExits(userIri, museumIri);
		List<String> mustSeeNodes = findMustSeePoisForUserInMuseum(userIri, museumIri);

		MuseumTourComputer mtg = new MuseumTourComputer(g);

		PersonalizedTour tour = null;
		boolean suitablePathFound = false;

		//take the first path suitable (i.e. not empty) for user.
		Iterator<String> entrancesIt = ee.entrances.iterator();
		Iterator<String> exitsIt = ee.exits.iterator();

		while(entrancesIt.hasNext() && !suitablePathFound) {

			String source = entrancesIt.next();
			while(exitsIt.hasNext() && !suitablePathFound) {

				String dest = exitsIt.next();

				tour = mtg.computeTour(mustSeeNodes, source, dest);

				if (!tour.isEmpty()) {
					suitablePathFound = true;
				}

			}
		}

		return tour;
	}


	private EntrancesAndExits findEntrancesAndExits(String userIri, String museumIri) {

		userIri = getATurtleValidUri(userIri);

		String getEntrancesAndExits = PREFIXES_STATEMENTS 
				+ "SELECT ?entrance ?exit \n"
				+ "WHERE {\n"
				+ museumIri +" iloc:hasEntrance ?entrance;\n"
				+ ":hasExit ?exit.\n"
				+ "?entrance :isSuitableFor " + userIri + " .\n"
				+ "?exit :isSuitableFor " + userIri + " .\n"
				+ "}\n"
				+ "";

		Set<String> entrances = new HashSet<>();
		Set<String> exits = new HashSet<>();
		Query query = QueryFactory.create(getEntrancesAndExits) ;
		try (QueryExecution qexec = QueryExecutionFactory.create(query, modelWithInferences)) {
			ResultSet results = qexec.execSelect();

			while (results.hasNext()){

				QuerySolution soln = results.nextSolution() ;
				entrances.add(soln.get("?entrance").toString().replace(omnisUri, omnisPrefix + ":"));
				exits.add(soln.get("?exit").toString().replace(omnisUri, omnisPrefix + ":"));

			}
		}
		return new EntrancesAndExits(entrances, exits);
	}

	static private class EntrancesAndExits {

		private Set<String> entrances;
		private Set<String> exits;

		private EntrancesAndExits(Set<String> entrances, Set<String> exits) {
			this.entrances = entrances;
			this.exits = exits;
		}
	}

	private String getATurtleValidUri(String iri) {
		boolean notPrefixedIri = iri.contains("//");

		if (notPrefixedIri) {
			iri = prepend(iri, "<");
			iri = iri.concat(">");
		}
		return iri;
	}

	private Graph buildGraphForUserInMuseum(String userIri2, String museumIri) {

		String userIri = getATurtleValidUri(userIri2);

		String userClass = retrieveUserClass(userIri);

		String graphConstr = PREFIXES_STATEMENTS
				+ "SELECT ?rs ?p1 ?p2 ?length ?incline ?numberOfSteps ?duration \n"
				+ "WHERE {\n"
				+ museumIri + " ecrm:P89i_contains ?p1 , ?p2.\n"

				+ "?rs a iloc:RouteSection;\n"
				+ ":isSuitableFor " + userIri + " ;\n"

				+ "airo:isLong ?lengthObject;\n"

				+ "airo:hasIncline ?inclineObject;\n"
				+ "airo:hasNumberOfSteps ?numberOfStepsObject;\n"

				+ "iloc:routeFromPOI ?p1;\n"
				+ "iloc:routeToPOI ?p2.\n"
				+ "?p1 :isSuitableFor " + userIri + ".\n"
				+ "?p2 :isSuitableFor " + userIri + ".\n"

				+ "?lengthObject om:hasNumericalValue ?length.\n"
				+ "?inclineObject om:hasNumericalValue ?incline.\n"
				+ "?numberOfStepsObject om:hasNumericalValue ?numberOfSteps.\n"

				+ "OPTIONAL{"
				+ "?rs airo:takesTime ?durationObject.\n"
				+ "?durationObject om:hasNumericalValue ?duration.\n"
				+ "}"
				+ "}\n";

		//graph construction query
		Query query = QueryFactory.create(graphConstr) ;

		try (QueryExecution qexec = QueryExecutionFactory.create(query, modelWithInferences)) {

			ResultSet results = qexec.execSelect();

			GraphBuilder gb = new GraphBuilder();

			while (results.hasNext()){
				QuerySolution soln = results.nextSolution() ;

				String p1 = soln.get("?p1").toString();
				String p2 = soln.get("?p2").toString();

				p1 = p1.replace(omnisUri, omnisPrefix + ":");	//for easing read
				p2 = p2.replace(omnisUri, omnisPrefix + ":");

				double length = soln.getLiteral("?length").getDouble();
				int incline = soln.getLiteral("?incline").getInt();
				int numberOfSteps = soln.getLiteral("?numberOfSteps").getInt();
				int duration = soln.getLiteral("?duration") != null ? soln.getLiteral("?duration").getInt() : 0;

				gb.addEdge(p1, p2, chooseMetricsForUserClass(userClass, length, incline, numberOfSteps, duration));
			}

			Graph g = gb.build();
			return g;
		}
	}

	private double chooseMetricsForUserClass(String userClass, double length, int incline, int numberOfSteps, int duration) {

		String prefixedUserClass = userClass.replace(omnisUri, omnisPrefix + ":");

		double lengthCoefficient = 0.0;
		double inclineCoefficient = 0.0;
		double numberOfStepsCoefficient = 0.0;
		double durationCoefficient = 0.0;

		switch (prefixedUserClass) {
		case ":NonImpairedUser":
			lengthCoefficient = 1.0;
			break;
		case ":WheelchairUser":
			lengthCoefficient = 1.0;
			inclineCoefficient = 1.0;
			numberOfStepsCoefficient = 3.0;
			break;
		case ":UserWithCrutches":
			lengthCoefficient = 1.0;
			inclineCoefficient = 0.5;
			numberOfStepsCoefficient = 3.0;
			break;
		case ":DeafUser":
			lengthCoefficient = 1.0;
			break;
		case ":BlindUser":
			lengthCoefficient = 1.0;
			numberOfStepsCoefficient = 1.5;			
			break;
		case ":LowVisionUser":
			lengthCoefficient = 1.0;
			numberOfStepsCoefficient = 1.5;			
			break;
		case ":ElderlyUser":
			lengthCoefficient = 1.5;
			numberOfStepsCoefficient = 1.0;
			break;
		case ":HurriedUser":
			lengthCoefficient = 0.2;
			durationCoefficient = 2.0;
			break;
		}

		return lengthCoefficient * length + 
				inclineCoefficient * incline +
				numberOfStepsCoefficient * numberOfSteps +
				durationCoefficient * duration;
	}


	private List<String> findMustSeePoisForUserInMuseum(String userIri, String museumIri) {
		userIri = getATurtleValidUri(userIri);

		String getUserData = PREFIXES_STATEMENTS
				+ "SELECT ?u (?durValue as ?timeForVisitValue)\n"
				+ "WHERE {\n"
				+ "?u :hasTimeForVisit ?dur.\n"
				+ "?dur om:hasUnit om:minute-Time.\n"
				+ "?dur om:hasNumericalValue ?durValue.\n"
				+ "}\n"
				+ "";

		int userTimeForVisit; int defaultVisitTime = 100;

		Query query = QueryFactory.create(getUserData);
		try (QueryExecution qexec = QueryExecutionFactory.create(query, modelWithInferences)) {
			ResultSet results = qexec.execSelect();
			userTimeForVisit = results.hasNext() ? results.next().getLiteral("timeForVisitValue").getInt() : defaultVisitTime;
		}

		List<String> mustVisitPois = new ArrayList<>();

		String getMustSees = PREFIXES_STATEMENTS
				+ "SELECT distinct ?room ?approximateTimeForVisitValue\n"
				+ "where {\n"
				+ "SELECT (SUM(?bonusOrPenaltyValue) as ?metrics) ?room ?approximateTimeForVisitValue \n"
				+ "where {\n"
				+ "  VALUES (?p ?bonusOrPenaltyValue) {\n"

				+ "      (:hasStylisticLowBonusFor \"0.1\")\n"
				+ "      (:hasStylisticMediumBonusFor \"0.2\")\n"
				+ "      (:hasStylisticHighBonusFor \"0.5\")\n"
				+ "      (:hasStylisticLowPenaltyFor \"-0.1\")\n"
				+ "      (:hasStylisticMediumPenaltyFor \"-0.2\")\n"
				+ "      (:hasStylisticHighPenaltyFor \"-0.3\")\n"

				+ "      (:hasArtistPreferenceLowBonusFor \"0.1\")\n"
				+ "      (:hasArtistPreferenceMediumBonusFor \"0.2\")\n"
				+ "      (:hasArtistPreferenceHighBonusFor \"0.5\")\n"
				+ "      (:hasArtistPreferenceLowPenaltyFor \"-0.1\")\n"
				+ "      (:hasArtistPreferenceMediumPenaltyFor \"-0.2\")\n"
				+ "      (:hasArtistPreferenceHighPenaltyFor \"-0.3\")\n"

				+ "      (:hasPeriodPreferenceLowBonusFor \"0.025\")\n"
				+ "      (:hasPeriodPreferenceMediumBonusFor \"0.05\")\n"
				+ "      (:hasPeriodPreferenceHighBonusFor \"0.125\")\n"
				+ "      (:hasPeriodPreferenceLowPenaltyFor \"-0.02\")\n"
				+ "      (:hasPeriodPreferenceMediumPenaltyFor \"-0.05\")\n"
				+ "      (:hasPeriodPreferenceHighPenaltyFor \"-0.075\")\n"

				+ "      (:hasSensorialLowBonusFor \"0.05\")\n"
				+ "      (:hasSensorialMediumBonusFor \"0.1\")\n"
				+ "      (:hasSensorialHighBonusFor \"0.25\")\n"
				+ "      (:hasSensorialLowPenaltyFor \"-0.05\")\n"
				+ "      (:hasSensorialMediumPenaltyFor \"-0.1\")\n"
				+ "      (:hasSensorialHighPenaltyFor \"-0.15\")\n"
				+ "  }\n"
				//+ "?p rdf:type owl:ObjectProperty. \n" //required by snap in sparql plugin (protege)
				+ "?room ecrm:P89_falls_within "+ museumIri +" ;\n"
				+ ":isSuitableFor " + userIri + " ;\n"
				+ ":requiresApproximateTimeForVisit ?approximateTimeForVisit;\n"
				+ ":hasSuitabilityMeasureFor " + userIri + " ;\n"
				+ "?p " + userIri + ".\n"
				+ "?approximateTimeForVisit om:hasUnit om:minute-Time; \n"
				+ "om:hasNumericalValue ?approximateTimeForVisitValue. \n"
				+ "} GROUP BY ?room ?approximateTimeForVisitValue\n"
				+ "} ORDER BY DESC(?metrics) \n"
				+ "";

		query = QueryFactory.create(getMustSees);
		try (QueryExecution qexec = QueryExecutionFactory.create(query, modelWithInferences)) {

			ResultSet results = qexec.execSelect();

			int cumulatedTime = 0;
			while (results.hasNext() && cumulatedTime < userTimeForVisit){

				QuerySolution soln = results.nextSolution();

				String mustVisitRoom = soln.get("?room").toString();
				mustVisitRoom = mustVisitRoom.replace(omnisUri, omnisPrefix + ":");
				int roomVisitingTime = soln.getLiteral("?approximateTimeForVisitValue").getInt();

				cumulatedTime += roomVisitingTime ;
				mustVisitPois.add(mustVisitRoom);
			}

			return mustVisitPois;
		}
	}

	private String retrieveUserClass(String userIri) {
		String userClassQuery = PREFIXES_STATEMENTS
				+ "select distinct ?cls where {\r\n"
				+ userIri + " a ?cls.?cls rdfs:subClassOf ?sup . \r\n"
				+ "FILTER NOT EXISTS{?sub rdfs:subClassOf ?cls  . " + userIri + " a ?sub. FILTER(?sub != ?cls && ?sub != owl:Nothing )}\r\n"
				+ "}\r\n"
				+ "";

		return getResultQueryAsList(userClassQuery, modelWithInferences, "?cls").get(0);
	}

	public Set<String> listAllUsers() {
		String usersQuery = PREFIXES_STATEMENTS 
				+ "select distinct ?user "
				+ "where {\n" 
				+ "?user a ?class ."
				+ "?class rdfs:subClassOf* uso:User."
				+ "}\r\n"
				+ "";
		return getResultQueryAsSet(usersQuery, modelWithInferences, "?user");
	}


	//Use case applied to Uffizi Museum of Firenze that shows:
	//1. OMNIS ontology capabilities in supporting the personalization of itineraries by providing a 
	//different personalized tour for every user in the knowledge base (based on his 
	//preferences, disabilities, capabilities, age etc.)
	//2. the algorithmic component capabilities in actually computing paths based on these OMNIS info.
	public static void main(String[] args) {

		OmnisManager man = new OmnisManager();

		String museumIri = "dbr:Uffizi";
		
		View gui = new View();
		
		gui.initialize();
		
		gui.display();

		gui.printStartClassifying();

		man.classify();
		
		gui.printEndClassifying();
		
		for (String userIri: man.listAllUsers()) {
			logln("Computing tour for user " + userIri + " ...");
			gui.printStartComputingFor(userIri);
			
			PersonalizedTour tour = man.computePersonalizedTourForUserInMuseum(userIri, museumIri);
			
			logln("...done. Personalized tour for user "+ userIri+ " between Uffizi rooms is: " + tour.getPath() + "\n\n and passing through " + tour.getMustSees() + "\n\n\n");
			gui.printTour(userIri, tour);
		}
		
		gui.printEndPersonalization();

	}

}