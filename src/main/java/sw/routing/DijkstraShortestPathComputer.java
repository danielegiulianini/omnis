package sw.routing;

import java.util.*;

import sw.routing.GraphBuilder.Graph;
import sw.routing.TspNearestNeighbourSolver.Walk;
import static sw.utils.Logging.log;



//Adapted from https://www.lavivienpost.com/shortest-path-and-2nd-shortest-path-using-dijkstra-code/

//An implementation of Dijkstra�s Shortest Path Algorithm
public class DijkstraShortestPathComputer {

	private final int NO_PARENT = -1;

	//Dijkstra�s Shortest Path Algorithm, O(n^2) Space O(n)
	public Walk computeShortestPath(double[][] adjacencyMatrix,  int src, int dest) {

		int n = adjacencyMatrix[0].length; 
		double[] shortest = new double[n]; 
		boolean[] visited = new boolean[n];
		int[] parents = new int[n];

		//initialization
		for (int v = 0; v < n; v++)  { 
			shortest[v] = Graph.INFINITE_COST; 
			visited[v] = false;
		} 
		shortest[src] = 0;
		parents[src] = NO_PARENT;

		//main loop
		for (int i = 1; i < n; i++){

			int pre = -1;

			double min = Graph.INFINITE_COST;

			//get minimum (shortest) from i (v is the dest)
			for (int v = 0;  v < n;  v++) { 
				if (!visited[v]) {
					if (shortest[v] < min){	//&& shortest[v] < Double.MAX_VALUE){

						pre = v;

						min = shortest[v]; 
					}
				}
			}
			
			//all neighbours visited and no more shortest found
			if(pre == -1) {	
				return visited[dest] ? 
						new Walk(reconstructPath(new ArrayList<>(), dest, parents), shortest[dest]) 
						: new Walk(new ArrayList<>(), Graph.INFINITE_COST);
			}

			visited[pre] = true;
			
			for (int v = 0; v < n; v++){
				double dist = adjacencyMatrix[pre][v];
				if (dist >= 0 && ((min + dist) < shortest[v])){	//author used >0
					parents[v] = pre;
					shortest[v] = min + dist;
				} 
			} 
		}

		return new Walk(reconstructPath(new ArrayList<>(), dest, parents), 
				shortest[dest]);
	} 

	//utility reconstruct the (shortest) path from source to destination recursively
	List<Integer> reconstructPath(List<Integer> path2, int i, int[] parents)  {
		if (i == NO_PARENT) {      	
			return path2;   	
		}
		reconstructPath(path2, parents[i], parents);             
		path2.add(i);
		return path2;
	}

	public static void main(String[] args) { 
		/*
		 *      0
		 *     / \ 
		 * (1)/   \(1)
		 *   /     \
		 *  3--(4)--1--(1)--2
		 */   	

		/*double graph2[][] = new double [][] {
			{0, 374, 200, 223, 108, 178, 252, 285, 240, 356},
			{374, 0, 255, 166, 433, 199, 135, 95, 136, 17},
			{200, 255, 0, 128, 277, 128, 180, 160, 131, 247},
			{223, 166, 128, 0, 430, 47, 52, 84, 40, 155},
			{108, 433 ,277, 430, 0, 453, 478, 344, 389, 423},
			{178, 199 ,128, 47, 453, 0, 91, 110, 64, 181},
			{252, 135 ,180, 52, 478, 91, 0, 114, 83, 117},
			{285, 95 ,160, 84, 344, 110, 114, 0, 47, 78},
			{240, 136 ,131, 40, 389, 64, 83, 47, 0, 118},
			{356, 17, 247, 155, 423, 181, 117, 78, 118, 0}};*/

		double[][] adjacencyMatrix = new double[][] {
			{ 0, 1, 5, 1},
			{ 1, 0, 1, Graph.INFINITE_COST},
			{ 0, 1, 0, Graph.INFINITE_COST},
			{ 1, 4, 0, Graph.INFINITE_COST}
		};

		int src = 1, dest = 3;		//int src = 0, dest = 7;

		Walk w = new DijkstraShortestPathComputer().computeShortestPath(adjacencyMatrix,src,dest); 
		log("shortest path between " + src +" and " + dest +": ");        
		log("" + w.getNodesIndexes());
		log("Shortest distance: " + w.getCost());

	}
}
