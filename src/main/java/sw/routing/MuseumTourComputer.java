package sw.routing;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import sw.routing.GraphBuilder.Graph;
import sw.routing.TspNearestNeighbourSolver.Walk;
import static sw.utils.Logging.logln;
import static sw.routing.GraphBuilder.fromAdjMatrToGraph;


public class MuseumTourComputer {

	private Graph museumPlanGraph;

	public MuseumTourComputer(Graph museumPlanGraph) {
		this.museumPlanGraph = museumPlanGraph;
	}

	public PersonalizedTour computeTour(List<String> mustSeeNodes, String source, String dest) {	//or a set of mustSeeNodes

		List<String> mustVisitNodes = new ArrayList<>(mustSeeNodes);

		mustVisitNodes.add(source);
		mustVisitNodes.add(dest);

		mustVisitNodes = mustVisitNodes.stream().filter(museumPlanGraph::contains).collect(Collectors.toList());

		Map<String, Map<String, Walk>> shortestWalks = computeShortestWalks(mustVisitNodes);
		Graph mustVisitNodesGraph = computeMustVisitNodesGraph(shortestWalks);

		Walk walk = new TspNearestNeighbourSolver().multiStartTsp(
				mustVisitNodesGraph.getNodeIndexFromNodeName(source),
				mustVisitNodesGraph.getNodeIndexFromNodeName(dest), 
				mustVisitNodesGraph.getAdiacencyMatrix());

		//reconverting from indexes to names to present them to user
		List<String> poiList = mustVisitNodesGraph.getNodeNamesFromAdjMatrixIndexes(walk.getNodesIndexes());

		//decompress must-see pois paths
		poiList = decompressPaths(poiList, shortestWalks);
		
		//could be empty if no walk is found by tsp
		return new PersonalizedTour(poiList, new HashSet<>(mustVisitNodes));
	}

	private Map<String, Map<String, Walk>> computeShortestWalks(List<String> mustVisitNodes){

		Map<String, Map<String, Walk>> walksMapByNodeNames = new LinkedHashMap<>();

		for (String node1 : mustVisitNodes) {
			for (String node2 : mustVisitNodes) {

				Walk walk = new DijkstraShortestPathComputer().computeShortestPath(
						museumPlanGraph.getAdiacencyMatrix(), 
						museumPlanGraph.getNodeIndexFromNodeName(node1), 
						museumPlanGraph.getNodeIndexFromNodeName(node2));

				//must fill intermediate structure (map) for actual (not condensed) paths
				walksMapByNodeNames.computeIfAbsent(node1, l -> new LinkedHashMap<>()).put(node2, walk);
			}
		}
		return walksMapByNodeNames;
	}

	private List<String> decompressPaths(List<String> compressed, Map<String, Map<String, Walk>> shortestWalks){

		List<String> decompressed = new ArrayList<>();

		if (compressed.size() > 0) {

			decompressed.add(compressed.get(0));

			//for every couple of must-sees: replace it with corresponding path of actual nodes that connects them
			for (int i = 0; i < compressed.size() - 1; i++) {

				Walk decompressedIndexPathBetweenTwoNodes = shortestWalks.get(compressed.get(i)).get(compressed.get(i+1));

				//path could not exist but [] is returned (by tsp) anyway
				List<String> decompressedNamePathBetweenTwoNodes = museumPlanGraph.getNodeNamesFromAdjMatrixIndexes(decompressedIndexPathBetweenTwoNodes.getNodesIndexes());

				decompressed.addAll(decompressedNamePathBetweenTwoNodes.subList(1, decompressedNamePathBetweenTwoNodes.size()));
			}
		}
		return decompressed;
	}


	private Graph computeMustVisitNodesGraph(Map<String, Map<String, Walk>> adjacencyMap) {
		GraphBuilder mustVisitNodesGraphBuilder = new GraphBuilder();

		adjacencyMap.forEach((node1, node2Map) -> 
		node2Map.forEach((node2, walk) ->
		mustVisitNodesGraphBuilder.addEdge(node1, node2, walk.getCost())));		
		return mustVisitNodesGraphBuilder.build();
	}


	public static class PersonalizedTour {
		
		private List<String> path;
		private Set<String> mustSees;

		public PersonalizedTour(List<String> path, Set<String> mustSees) {
			this.path = path;
			this.mustSees = mustSees;
		}
		
		public boolean isEmpty() {
			return path.isEmpty();
		}

		public List<String> getPath() {
			return path;
		}

		public Set<String> getMustSees() {
			return mustSees;
		}
	}

	public static void main(String[] args) {
		//test of application workflow

		double adjacency_matrix[][] = new double [][] {
			{0, 374, 200, 223, 108, 178, 252, 285, 240, 356},
			{374, 0, 255, 166, 433, 199, 135, 95, 136, 17},
			{200, 255, 0, 128, 277, 128, 180, 160, 131, 247},
			{223, 166, 128, 0, 430, 47, 52, 84, 40, 155},
			{108, 433 ,277, 430, 0, 453, 478, 344, 389, 423},
			{178, 199 ,128, 47, 453, 0, 91, 110, 64, 181},
			{252, 135 ,180, 52, 478, 91, 0, 114, 83, 117},
			{285, 95 ,160, 84, 344, 110, 114, 0, 47, 78},
			{240, 136 ,131, 40, 389, 64, 83, 47, 0, 118},
			{356, 17, 247, 155, 423, 181, 117, 78, 118, 0}};

			Graph g = fromAdjMatrToGraph(adjacency_matrix);

			String source = g.getNodeNameFromAdjMatrixIndex(0);
			String dest = g.getNodeNameFromAdjMatrixIndex(6);
			List<String> mustSeeNodes= new ArrayList<>();
			mustSeeNodes.add(g.getNodeNameFromAdjMatrixIndex(2));
			mustSeeNodes.add(g.getNodeNameFromAdjMatrixIndex(4));
			mustSeeNodes.add(g.getNodeNameFromAdjMatrixIndex(7));

			MuseumTourComputer mtg = new MuseumTourComputer(g);
			List<String> path = mtg.computeTour(mustSeeNodes, source, dest).getPath();

			logln("the shortest path: \n\tstarting from " + source + ",\n\tending in: " 
					+ dest + " \n\tand passing (not in any predetermined order) through: " 
					+ mustSeeNodes + "\nis: " + path);
	}

}
