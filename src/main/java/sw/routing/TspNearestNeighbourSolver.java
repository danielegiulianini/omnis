package sw.routing;

import static sw.utils.Logging.printTwoDimensionalArrayOfPrimitiveDoubles;
import static sw.utils.Logging.logln;
import static sw.utils.ArrayUtils.appendElemToArrayOfPrimitiveDoubles;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import sw.routing.GraphBuilder.Graph;



public class TspNearestNeighbourSolver {

	private double DUMMY_NODE_COST = -1;

	public Walk tsp(int start, double[][] adjMatr) {
		int numberOfNodes = adjMatr.length;
		boolean[] visited = new boolean[numberOfNodes];
		visited[start] = true;
		Walk walk = graphTraversal(start, visited, adjMatr, true, 0);
		return walk;
	}

	public Walk tsp(double[][] adjMatr) {
		return tsp(0, adjMatr);
	}

	public Walk multiStartTsp(int source, int dest, double[][] adjMatr) {
		double[][] adjustedAdjMatr = addDummyNodeToAdjacencyMatrix(adjMatr, source, dest);

		double minimumCost = Graph.INFINITE_COST;
		Walk walk = null;

		for (int i = 0; i < adjMatr.length; i++) {
			walk = tsp(i, adjustedAdjMatr);

			List<Integer> indexesNodes = walk.getNodesIndexes();
			boolean walkFound = indexesNodes.size() == adjustedAdjMatr.length;
			if (walkFound) {
				double toSumCost = adjustedAdjMatr[indexesNodes.get(indexesNodes.size()-1)][indexesNodes.get(0)];
				
				Collections.rotate(indexesNodes, indexesNodes.size() - indexesNodes.indexOf(source));
				
				//remove cost for edges incident with dummy node
				double toSubCost = 2 * DUMMY_NODE_COST;
				
				//remove dummy node and corresponding edges
				int dummyNodeIndex = adjustedAdjMatr.length -1;
				indexesNodes.remove(indexesNodes.indexOf(dummyNodeIndex));
				if (walk.getCost() < minimumCost) {
					minimumCost = walk.getCost();
				}
				walk = new Walk(indexesNodes, walk.getCost() + toSumCost - toSubCost );
			} else {
				walk = new Walk(Collections.emptyList(), 0);
			}
		}

		return walk;
	}

	private Walk graphTraversal(int u, boolean[] visited, double[][] adjMatr, boolean aWalkableEdgeFoundInPreviousIteration, double walkCost) {
		
		double currentWalkCost = walkCost;

		if (aWalkableEdgeFoundInPreviousIteration){
			boolean aWalkableEdgeFoundInCurrentIteration = false;
			double minimumEdge = Graph.INFINITE_COST;
			int minimumNode = u;
			
			visited[u] = true;

			for (int i = 0; i < adjMatr.length; i++) {
				if(adjMatr[u][i] < minimumEdge 
						&& !visited[i]){ 
					minimumEdge = adjMatr[u][i];
					minimumNode = i;
					aWalkableEdgeFoundInCurrentIteration = true;
				} else {
					//no solution (some nodes still to be visited but no non-inf edge found to get to them)
				}
			}
			if (aWalkableEdgeFoundInCurrentIteration) {
				currentWalkCost += minimumEdge;
			}

			Walk res = graphTraversal(minimumNode, visited, adjMatr, aWalkableEdgeFoundInCurrentIteration, currentWalkCost);
			List<Integer> newNodesIndexes = new ArrayList<>();
			newNodesIndexes.add(u);
			newNodesIndexes.addAll(res.getNodesIndexes());
			return new Walk(newNodesIndexes, res.getCost());
		} else {
			return new Walk(Collections.emptyList(), currentWalkCost);
		}
	}

	private double[][] addDummyNodeToAdjacencyMatrix(double[][] adjMatr, int source, int dest) {
		int number_of_nodes = adjMatr.length - 1;

		double new_adjacency_matrix[][] = new double[number_of_nodes + 2][number_of_nodes + 2];//2: 1 for node to itself, 1 for node to dummy

		//adding last column
		for (int i = 0; i< adjMatr.length; i++) {	
			double elem = i == dest ? -1 : Graph.INFINITE_COST;
			new_adjacency_matrix[i] = appendElemToArrayOfPrimitiveDoubles(adjMatr[i], elem);
		}
		//adding last row
		for (int i = 0; i < new_adjacency_matrix.length; i++){
			double elem = i == source ? -1 : Graph.INFINITE_COST;
			new_adjacency_matrix[new_adjacency_matrix.length-1][i] = elem;
		}
		return new_adjacency_matrix;
	}

	static class Walk {
		private List<Integer> nodesIndexes;
		private double cost;

		public Walk(List<Integer> nodesIndexes, double cost) {
			this.nodesIndexes = nodesIndexes;
			this.cost = cost;
		}

		public double getCost() {
			return cost;
		}

		public List<Integer> getNodesIndexes() {
			return new ArrayList<>(nodesIndexes);
		}
	}

	public static void main(String[] args) {

		double adjacency_matrix[][] = new double [][] {
			{0, 374, 200, 223, 108, 178, 252, 285, 240, 356},
			{374, 0, 255, 166, 433, 199, 135, 95, 136, 17},
			{200, 255, 0, 128, 277, 128, 180, 160, 131, 247},
			{223, 166, 128, 0, 430, 47, 52, 84, 40, 155},
			{108, 433 ,277, 430, 0, 453, 478, 344, 389, 423},
			{178, 199 ,128, 47, 453, 0, 91, 110, 64, 181},
			{252, 135 ,180, 52, 478, 91, 0, 114, 83, 117},
			{285, 95 ,160, 84, 344, 110, 114, 0, 47, 78},
			{240, 136 ,131, 40, 389, 64, 83, 47, 0, 118},
			{356, 17, 247, 155, 423, 181, 117, 78, 118, 0}};
		
		int source = 0, dest = 3;

		logln("matrix: ");
		printTwoDimensionalArrayOfPrimitiveDoubles(adjacency_matrix);
		
		logln("computing a tour from " + source + " and " + dest + "...");
		
		TspNearestNeighbourSolver  tspNearestNeighbour = new TspNearestNeighbourSolver();
		Walk sw = tspNearestNeighbour.multiStartTsp(source, dest, adjacency_matrix);

		logln("walk between "+ source +" and "+  dest+ ": " + sw.getNodesIndexes() + ", with total cost:" + sw.getCost());

	}

}
