package sw.routing;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


import static sw.utils.Logging.printTwoDimensionalArrayOfPrimitiveDoubles;
import static sw.utils.MyStringUtils.rpad;

/* 
 * GoF's Builder pattern applied to immutable Graph (with double, and possibly asymmetric, weights) creation by a fluent API 
 * and that keeps track of the corresponding adjacency matrix (as bi-dimensional java array) and the mapping between 
 * its indexes and weight costs. Useful for reusing some algorithms and code already developed for the aforementioned
 * and more popular graph representation of bi-dimensional array (ex. double[][]).
 * */
public class GraphBuilder {

	private int currentIndex;
	private Map<Integer, Map<Integer, Double>> adjacencyMap;
	private Map<String, Integer> namesToAdjacencyMatrixIndexes;
	private Map<Integer, String> adjacencyMatrixIndexesToNames;

	public GraphBuilder() {
		this.currentIndex = 0;
		this.adjacencyMap = new LinkedHashMap<>();	//for preserving insertion order in keys
		this.namesToAdjacencyMatrixIndexes = new HashMap<>();
		this.adjacencyMatrixIndexesToNames = new HashMap<>();
	}

	public GraphBuilder addEdge(String fromNodeName, String toNodeName, double edgeWeight) {
		int fromNodeindex = assignIndex(fromNodeName);
		int toNodeindex = assignIndex(toNodeName);

		adjacencyMap.
		computeIfAbsent(fromNodeindex, 
				l -> new LinkedHashMap<>())
		.put(toNodeindex, edgeWeight);//or putIfAbsent if not want to override previous edge
		return this;
	}

	private int assignIndex(String nodeName) {
		return namesToAdjacencyMatrixIndexes.computeIfAbsent(nodeName, e -> {
			adjacencyMatrixIndexesToNames.putIfAbsent(currentIndex, nodeName);
			return currentIndex++;
		});
	}

	public Graph build() {
		
		double[][] adjacencyMatrix = new double[currentIndex][currentIndex];

		//edges not explicitly set have infinite cost
		for (int i = 0; i < adjacencyMatrix.length; i++) { 
			for (int j = 0; j < adjacencyMatrix[i].length; j++) { 
				adjacencyMatrix[i][j] = i==j ? 0 : Graph.INFINITE_COST;//INF_COST-1; 
			} 
		}

		adjacencyMap.keySet().forEach(k1 -> {
			adjacencyMap.get(k1).entrySet().forEach(e ->
			adjacencyMatrix[k1][e.getKey()] = e.getValue());
		});
		
		return new Graph(namesToAdjacencyMatrixIndexes,adjacencyMatrixIndexesToNames, 
				adjacencyMatrix);
	}


	public static Graph fromAdjMatrToGraph(double adjacency_matrix[][]) {
		GraphBuilder gb = new GraphBuilder();
		for (int i = 0; i<adjacency_matrix.length; i++){
			for (int j = 0; j<adjacency_matrix.length; j++){
				gb.addEdge("a" + i, "a" + j, adjacency_matrix[i][j]);
			}
		}
		return gb.build();
	}


	public static class Graph {

		public static final double INFINITE_COST = Double.MAX_VALUE;
		
		private Map<String, Integer> namesToAdjacencyMatrixIndexes;
		private Map<Integer, String> adjacencyMatrixIndexesToNames;
		private double[][] adjacencyMatrix;

		public Graph(Map<String, Integer> namesToAdjacencyMatrixIndexes, 
				Map<Integer, String> adjacencyMatrixIndexesToNames, 
				double[][] adjacencyMatrix) {
			this.namesToAdjacencyMatrixIndexes = namesToAdjacencyMatrixIndexes;
			this.adjacencyMatrixIndexesToNames = adjacencyMatrixIndexesToNames;
			this.adjacencyMatrix = adjacencyMatrix;
		}

		public double[][] getAdiacencyMatrix(){
			return adjacencyMatrix;
		}

		public int getNodeIndexFromNodeName(String nodeName) {
			return namesToAdjacencyMatrixIndexes.get(nodeName);
		}

		public String getNodeNameFromAdjMatrixIndex(int i) {
			return adjacencyMatrixIndexesToNames.get(i);
		}
		
		public boolean contains(String nodeName) {
			return namesToAdjacencyMatrixIndexes.get(nodeName)!=null;
		}
		
		public List<String> getNodeNamesFromAdjMatrixIndexes(List<Integer> indexes){
			return indexes.stream()
					.map(this::getNodeNameFromAdjMatrixIndex)
					.collect(Collectors.toList());
		}

		@Override
		public String toString() {
			int longestName = 47;	//should compute longest name length instead
			int separatingSpacesCount = 3;
			int columnWidth =longestName+ separatingSpacesCount;
			StringBuilder sb = new StringBuilder();
			sb.append(rpad("", columnWidth));
			for (int i = 0; i<adjacencyMatrix.length; i++) {
				sb.append(rpad(this.getNodeNameFromAdjMatrixIndex(i), columnWidth));
			}
			sb.append("\n");
			for (int j=0; j< adjacencyMatrix.length; j++){
				sb.append(rpad(this.getNodeNameFromAdjMatrixIndex(j), columnWidth));
				for (double element : adjacencyMatrix[j]) {
					sb.append(rpad("" + element, columnWidth));
				}
				sb.append("\n");
			}
			return sb.toString();
		}

	}


	//Some use cases for testing and showing how to use GraphBuilder and Graph
	public static void main(String[] args) {

		//test for not increasing index count if node already present
		String s1 = "s1", s2 ="s2";
		assert new GraphBuilder()
		.addEdge(s1, s2, 1.0)
		.addEdge(s1, s2, 3.0)
		.addEdge(s1, s2, 4.0).build().adjacencyMatrix.length == 2;

		//test to check overriding of edge in adj matrix
		Graph g = new GraphBuilder()
				.addEdge(s1, s2, 1.0)
				.addEdge(s2, s1, 3.0)
				.addEdge(s1, s2, 4.0).build();
		
		assert g.adjacencyMatrix[g.getNodeIndexFromNodeName(s1)][g.getNodeIndexFromNodeName(s2)] > 3.0;
		printTwoDimensionalArrayOfPrimitiveDoubles(g.adjacencyMatrix); //should show 4.0 between s1 and s2

		//test to check adj matrix correctness with more than 2 nodes
		String s4 = "s4";
		Graph g2 = new GraphBuilder()
				.addEdge(s1, s2, 1.0)
				.addEdge(s2, s1, 3.0)
				.addEdge(s1, s2, 4.0)
				.addEdge(s1, s4, 5.0).build();

		printTwoDimensionalArrayOfPrimitiveDoubles(g2.adjacencyMatrix);
	}


}


